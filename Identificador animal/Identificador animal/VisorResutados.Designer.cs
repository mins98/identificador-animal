﻿namespace Identificador_animal
{
    partial class VisorResutados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbVisor = new System.Windows.Forms.PictureBox();
            this.btnAnte = new System.Windows.Forms.Button();
            this.btnSig = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblAnimal = new System.Windows.Forms.Label();
            this.lblCert = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbVisor)).BeginInit();
            this.SuspendLayout();
            // 
            // pbVisor
            // 
            this.pbVisor.Location = new System.Drawing.Point(127, 67);
            this.pbVisor.Name = "pbVisor";
            this.pbVisor.Size = new System.Drawing.Size(311, 243);
            this.pbVisor.TabIndex = 0;
            this.pbVisor.TabStop = false;
            // 
            // btnAnte
            // 
            this.btnAnte.Location = new System.Drawing.Point(46, 172);
            this.btnAnte.Name = "btnAnte";
            this.btnAnte.Size = new System.Drawing.Size(75, 39);
            this.btnAnte.TabIndex = 1;
            this.btnAnte.Text = "Anterior";
            this.btnAnte.UseVisualStyleBackColor = true;
            this.btnAnte.Click += new System.EventHandler(this.btnAnte_Click);
            // 
            // btnSig
            // 
            this.btnSig.Location = new System.Drawing.Point(444, 172);
            this.btnSig.Name = "btnSig";
            this.btnSig.Size = new System.Drawing.Size(82, 39);
            this.btnSig.TabIndex = 2;
            this.btnSig.Text = "Siguiente";
            this.btnSig.UseVisualStyleBackColor = true;
            this.btnSig.Click += new System.EventHandler(this.btnSig_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(152, 328);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Animal: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(152, 357);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Certeza:";
            // 
            // lblAnimal
            // 
            this.lblAnimal.AutoSize = true;
            this.lblAnimal.Location = new System.Drawing.Point(228, 328);
            this.lblAnimal.Name = "lblAnimal";
            this.lblAnimal.Size = new System.Drawing.Size(20, 17);
            this.lblAnimal.TabIndex = 5;
            this.lblAnimal.Text = "...";
            // 
            // lblCert
            // 
            this.lblCert.AutoSize = true;
            this.lblCert.Location = new System.Drawing.Point(228, 357);
            this.lblCert.Name = "lblCert";
            this.lblCert.Size = new System.Drawing.Size(20, 17);
            this.lblCert.TabIndex = 6;
            this.lblCert.Text = "...";
            // 
            // VisorResutados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 425);
            this.Controls.Add(this.lblCert);
            this.Controls.Add(this.lblAnimal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSig);
            this.Controls.Add(this.btnAnte);
            this.Controls.Add(this.pbVisor);
            this.Name = "VisorResutados";
            this.Text = "VisorResutados";
            this.Load += new System.EventHandler(this.VisorResutados_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbVisor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbVisor;
        private System.Windows.Forms.Button btnAnte;
        private System.Windows.Forms.Button btnSig;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblAnimal;
        private System.Windows.Forms.Label lblCert;
    }
}