﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Identificador_animal
{
    public partial class VisorResutados : Form
    {
        public static List<Convertir> respuestas;
        private int nActual=1;
        public VisorResutados()
        {
            InitializeComponent();
  
        }

        private void btnAnte_Click(object sender, EventArgs e)
        {
            if (nActual - 1 <= 0)
            {
                nActual = respuestas.Count;
                cargarImagen();
            }
            else
            {
                nActual=nActual-1;
                cargarImagen();
            }
        }

        private void btnSig_Click(object sender, EventArgs e)
        {
            if(nActual+1>respuestas.Count)
            {
                nActual = 1;
                cargarImagen();
            }
            else
            {
                nActual++;
                cargarImagen();
            }
        }

        private void VisorResutados_Load(object sender, EventArgs e)
        {
            cargarImagen();
        }
        public void cargarImagen()
        {
            Image imagen = Image.FromFile("C:\\VideoFrames\\frame" + nActual + ".jpeg");
            pbVisor.Image = imagen;
            pbVisor.SizeMode = PictureBoxSizeMode.StretchImage;
            lblAnimal.Text = respuestas[nActual - 1].animal_pre;
            lblCert.Text = respuestas[nActual - 1].certeza;
        }
    }
}
