﻿namespace Identificador_animal
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnRecurso = new System.Windows.Forms.Button();
            this.pbImagen = new System.Windows.Forms.PictureBox();
            this.vdVideo = new AForge.Controls.VideoSourcePlayer();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.btnEnviar = new System.Windows.Forms.Button();
            this.btnVideo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagen)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRecurso
            // 
            this.btnRecurso.Location = new System.Drawing.Point(13, 23);
            this.btnRecurso.Name = "btnRecurso";
            this.btnRecurso.Size = new System.Drawing.Size(146, 41);
            this.btnRecurso.TabIndex = 0;
            this.btnRecurso.Text = "Cargar Recurso";
            this.btnRecurso.UseVisualStyleBackColor = true;
            this.btnRecurso.Click += new System.EventHandler(this.btnRecurso_Click);
            // 
            // pbImagen
            // 
            this.pbImagen.BackColor = System.Drawing.Color.Transparent;
            this.pbImagen.Enabled = false;
            this.pbImagen.Location = new System.Drawing.Point(277, 23);
            this.pbImagen.Name = "pbImagen";
            this.pbImagen.Size = new System.Drawing.Size(423, 313);
            this.pbImagen.TabIndex = 1;
            this.pbImagen.TabStop = false;
            this.pbImagen.Visible = false;
            // 
            // vdVideo
            // 
            this.vdVideo.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.vdVideo.Enabled = false;
            this.vdVideo.Location = new System.Drawing.Point(277, 23);
            this.vdVideo.Name = "vdVideo";
            this.vdVideo.Size = new System.Drawing.Size(423, 313);
            this.vdVideo.TabIndex = 2;
            this.vdVideo.Text = "videoSourcePlayer1";
            this.vdVideo.VideoSource = null;
            this.vdVideo.Visible = false;
            this.vdVideo.NewFrame += new AForge.Controls.VideoSourcePlayer.NewFrameHandler(this.vdVideo_NewFrame);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // btnEnviar
            // 
            this.btnEnviar.Enabled = false;
            this.btnEnviar.Location = new System.Drawing.Point(277, 372);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(120, 26);
            this.btnEnviar.TabIndex = 3;
            this.btnEnviar.Text = "Enviar Imagen";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // btnVideo
            // 
            this.btnVideo.Enabled = false;
            this.btnVideo.Location = new System.Drawing.Point(524, 369);
            this.btnVideo.Name = "btnVideo";
            this.btnVideo.Size = new System.Drawing.Size(115, 33);
            this.btnVideo.TabIndex = 5;
            this.btnVideo.Text = "Procesar video";
            this.btnVideo.UseVisualStyleBackColor = true;
            this.btnVideo.Click += new System.EventHandler(this.btnVideo_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(747, 512);
            this.Controls.Add(this.btnVideo);
            this.Controls.Add(this.btnEnviar);
            this.Controls.Add(this.vdVideo);
            this.Controls.Add(this.pbImagen);
            this.Controls.Add(this.btnRecurso);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Identificador Animal";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbImagen)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRecurso;
        private System.Windows.Forms.PictureBox pbImagen;
        private AForge.Controls.VideoSourcePlayer vdVideo;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.Button btnVideo;
    }
}

