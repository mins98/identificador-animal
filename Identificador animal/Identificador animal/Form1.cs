﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.FFMPEG;
using AForge.Video.DirectShow;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Net;
using AForge.Controls;

namespace Identificador_animal
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public Image imagen;
        public string ImagenDir;
        public FileVideoSource video;
        private Stopwatch stopWatch = null;
        private List<Image> imagenes;
        byte contador=0;
        byte intervalo = 20;
        string direccionWeb = @"http://127.0.0.1:5000/picture";

        bool shot = false;
        Loading load;
        private void btnRecurso_Click(object sender, EventArgs e)
        {
            timer.Stop();
            pbImagen.Visible = false;
            vdVideo.Visible = false;
            CloseCurrentVideoSource();
            try
            {
             
                OpenFileDialog opn = new OpenFileDialog();
                opn.Filter = "Imagenes|*.jpg;*.jpeg;*.png; | Videos(*.avi)|*.avi";
                if (opn.ShowDialog() == DialogResult.OK)
                {
                    if (Path.GetExtension(opn.FileName) == ".jpg"|| Path.GetExtension(opn.FileName) == ".jpeg" || Path.GetExtension(opn.FileName) == ".png")
                    {
                        pbImagen.Visible = true;
                        ImagenDir= opn.FileName;
                        imagen= System.Drawing.Image.FromFile(opn.FileName);
                        pbImagen.Image = imagen;
                        pbImagen.SizeMode = PictureBoxSizeMode.StretchImage;
                        video = null;
                        btnEnviar.Enabled = true;
                        btnVideo.Enabled = false;

                    }
                    else if (Path.GetExtension(opn.FileName) == ".avi")
                    {
                        imagen = null;
                        video= new FileVideoSource(opn.FileName);
                        OpenVideoSource(video);
                        btnEnviar.Enabled = false;
                        btnVideo.Enabled = true;
                    }
                }
            }            
            catch (Exception ex)
            {

                MessageBox.Show("Error al cargar imagen  " + ex.Message);
            }
        }
        private void OpenVideoSource(IVideoSource source)
        {
            vdVideo.Visible = true;
            this.Cursor = Cursors.WaitCursor;
            CloseCurrentVideoSource();
            vdVideo.VideoSource = source;
            vdVideo.Stop();
                    

            this.Cursor = Cursors.Default;
        }
        private void CloseCurrentVideoSource()
        {
            if (vdVideo.VideoSource != null)
            {
                vdVideo.SignalToStop();

                // wait ~ 3 seconds
                for (int i = 0; i < 30; i++)
                {
                    if (!vdVideo.IsRunning)
                        break;
                    System.Threading.Thread.Sleep(100);
                }

                if (vdVideo.IsRunning)
                {
                    vdVideo.Stop();
                }

                vdVideo.VideoSource = null;
            }
        }
        private void vdVideo_NewFrame(object sender, ref Bitmap image)
        {
            DateTime now = DateTime.Now;
            Graphics g = Graphics.FromImage(image);

            // paint current time
            SolidBrush brush = new SolidBrush(Color.Red);
            g.DrawString(now.ToString(), this.Font, brush, new PointF(5, 5));
            brush.Dispose();
            g.Dispose();
            if(shot)
            {
                Bitmap frame = (Bitmap)image.Clone();
        
                imagenes.Add(frame);
                shot = false;
                
            }
            
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            WebClient client = new WebClient();
            client.Credentials = CredentialCache.DefaultCredentials;
            byte[] res=client.UploadFile(direccionWeb, "POST", ImagenDir);
            string s = System.Text.Encoding.Default.GetString(res);
            client.Dispose();
            Convertir traducido = traductor(s);
            MessageBox.Show("Se predijo que el animal es un: "+ traducido.animal_pre+ " con una certeza de: "+traducido.certeza);

        }

        public Convertir traductor(string json)
        {
            Convertir convertido  = JsonConvert.DeserializeObject<Convertir>(json);
            return convertido;
        }

        private void btnVideo_Click(object sender, EventArgs e)
        {
            vdVideo.Start();
            stopWatch = null;
            timer.Start();
            imagenes = new List<Image>();
           
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            contador++;
            if (contador == intervalo)
            {
                contador = 0;
                shot = true;
            }
            if (!vdVideo.IsRunning)
            {
                timer.Stop();
                contador = 0;
                MessageBox.Show("Procesando imagenes");
                procesarVIdeo();
            }


        }
        private async void  procesarVIdeo()
        {
            int fCount = Directory.GetFiles("C:\\VideoFrames", "*", SearchOption.AllDirectories).Length;
            for (int i = 1; i <= fCount; i++)
            {
                File.Delete("C:\\VideoFrames\\frame" + i + ".jpeg");
            }
            int nImagen = 0;
            foreach (Image frame in imagenes)
            {
                nImagen++;
                string nombre = "C:\\VideoFrames\\frame" + nImagen + ".jpeg";
                frame.Save(nombre, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            VentanaEspera();
            Task<List<Convertir>> tarea = new Task<List<Convertir>>(calcular);
            tarea.Start();
            await tarea;
            CerrarEspera();
            MostrarRespuesta(tarea.Result);
        }
        private List<Convertir> calcular()
        {
            WebClient client = new WebClient();
            client.Credentials = CredentialCache.DefaultCredentials;

            List<Convertir> respuestas = new List<Convertir>();
            int fCount = Directory.GetFiles("C:\\VideoFrames", "*", SearchOption.AllDirectories).Length;
            for (int i = 1; i <= fCount; i++)
            {
                string nombre = "C:\\VideoFrames\\frame" + i + ".jpeg";
                imagen = Image.FromFile(nombre);
                byte[] res = client.UploadFile(direccionWeb, "POST", nombre);
                string s = System.Text.Encoding.Default.GetString(res);
                Convertir traducido = traductor(s);
                respuestas.Add(traducido);
            }
            client.Dispose();
            return respuestas;
           
        }
        private void MostrarRespuesta(List<Convertir> respuestas)
        {
            VisorResutados vs = new VisorResutados();
            VisorResutados.respuestas = respuestas;
            vs.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void VentanaEspera()
        {
           load = new Loading();
           load.Show();

        }
        private void CerrarEspera()
        {
            if(load!=null)
            {
                load.Close();
            }

        }
    }
}


          
