from flask import Flask, request
from flask_restful import  reqparse, Resource, Api

import os, uuid, werkzeug

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator


import numpy as np
import matplotlib.pyplot as plt

from tensorflow import keras

from PIL import Image  
import PIL  


app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('texto')

parser.add_argument('file', type=werkzeug.datastructures.FileStorage, location='files')

#variables globales
PATH = 'C:\Conda'
PATH=os.path.join(PATH, 'raw-img')
train_dir = os.path.join(PATH, 'training')
validation_dir = os.path.join('C:\Conda', 'image')
batch_size = 128
IMG_HEIGHT = 150
IMG_WIDTH = 150
train_image_generator = ImageDataGenerator(rescale=1./255) 
validation_image_generator = ImageDataGenerator(rescale=1./255)
train_data_gen = ""

class Picture(Resource):
    def post(self):
        #recuperar imagenn
        args = parser.parse_args()
        print(args)
        picture = args['file']
        im1 = picture 
        filename = str('fotoAnalisar' + '.jpg')
        file_path = os.path.join('image','pr', filename)
        im1 = im1.save(file_path) 

        #recuperar datos para predecir
        val_data_gen = validation_image_generator.flow_from_directory(batch_size=batch_size,
                                                              directory=validation_dir,
                                                              target_size=(IMG_HEIGHT, IMG_WIDTH),
                                                              class_mode='categorical')
        classes_dictionary = train_data_gen.class_indices 
        class_keys = list(classes_dictionary.keys())      
        #predecir
        new_model = keras.models.load_model('SinEleMariOve_10_88.h5')
        imagenAn,malId = next(val_data_gen)
        predictions = new_model.predict(imagenAn)
        pre=np.argmax(predictions[0])

        labelOriginal=np.where(malId[0] == 1)[0]
        #print ('Label original ',labelOriginal)
        print ('Label Predecido ',pre)
        #print('Animal Original',class_keys[labelOriginal[0]])
        animalPre=class_keys[pre];
        print('Animal Predecido',animalPre)
        cert=100*np.max(predictions[0])
        print ('Certeza de prediccion : ',cert)


        return {'animal_pre':animalPre, 'certeza':cert }


class Prueba(Resource):
    def get(self):
        return {'hello': 'world'}

class Postes(Resource):
    def post(self):
        args = parser.parse_args()
        asa = args['texto']      
        return {'response': asa,'keys': asa, 
        'animal_pre':asa, 'certeza':asa }

api.add_resource(Picture, '/picture')
api.add_resource(Prueba, '/Prueba')
api.add_resource(Postes, '/Postes')

if __name__ == '__main__':
    train_data_gen = train_image_generator.flow_from_directory(batch_size=batch_size,
                                                           directory=train_dir,
                                                           shuffle=True,
                                                           target_size=(IMG_HEIGHT, IMG_WIDTH),
                                                           class_mode='categorical')
    app.run(debug=True)